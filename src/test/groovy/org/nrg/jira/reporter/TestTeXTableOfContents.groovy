package org.nrg.jira.reporter

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.jira.reporter.component.TeXTableOfContents
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestTeXTableOfContents {

    private static final Execution execution1 = new Execution(issueKey : 'XNAT-100', summary : 'User Registration Test')
    private static final Execution execution2 = new Execution(issueKey : 'XNAT-101', summary : 'Project Creation Test')
    private static final Cycle cycle = new Cycle(executions : [execution1, execution2])

    @Test
    void testAllPassing() {
        execution1.setStatus(TestStatus.PASS)
        execution2.setStatus(TestStatus.PASS)
        assertEquals(FileIOUtils.loadResource('all_passing_example.tex').text, new TeXTableOfContents(cycle).generateTeX())
    }

    @Test
    void testNoneBlocked() {
        execution1.setStatus(TestStatus.FAIL)
        execution2.setStatus(TestStatus.PASS)
        assertEquals(FileIOUtils.loadResource('none_blocked_example.tex').text, new TeXTableOfContents(cycle).generateTeX())
    }

    @Test
    void testNoneFailing() {
        execution1.setStatus(TestStatus.BLOCKED)
        execution2.setStatus(TestStatus.BLOCKED)
        assertEquals(FileIOUtils.loadResource('none_failing_example.tex').text, new TeXTableOfContents(cycle).generateTeX())
    }

    @Test
    void testBlockedAndFailing() {
        execution1.setStatus(TestStatus.FAIL)
        execution2.setStatus(TestStatus.BLOCKED)
        assertEquals(FileIOUtils.loadResource('blocked_and_failing_example.tex').text, new TeXTableOfContents(cycle).generateTeX())
    }

}
