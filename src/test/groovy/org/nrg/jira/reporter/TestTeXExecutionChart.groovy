package org.nrg.jira.reporter

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.jira.reporter.component.TeXExecutionChart
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestTeXExecutionChart {

    @Test
    void testAllPassingChart() {
        assertEquals(FileIOUtils.loadResource('all_passing_chart_example.tex').text, new TeXExecutionChart(mockCycle(100, 0, 0, 0, 0)).generateTeX())
    }

    @Test
    void testRealisticChart() {
        assertEquals(FileIOUtils.loadResource('realistic_chart_example.tex').text, new TeXExecutionChart(mockCycle(100, 35, 10, 1, 20)).generateTeX())
    }

    private static Cycle mockCycle(int numPassing, int numFailing, int numBlocked, int numWip, int numUnexec) {
        final List<Execution> executions = []
        numPassing.times { executions << new Execution(status : TestStatus.PASS)}
        numFailing.times { executions << new Execution(status : TestStatus.FAIL)}
        numBlocked.times { executions << new Execution(status : TestStatus.BLOCKED)}
        numWip.times { executions << new Execution(status : TestStatus.WIP)}
        numUnexec.times { executions << new Execution(status : TestStatus.UNEXECUTED)}
        new Cycle(executions : executions)
    }

}
