package org.nrg.jira.reporter

import org.testng.annotations.Test

import static org.nrg.jira.reporter.TeXUtils.*
import static org.testng.AssertJUnit.*

class TestTeXUtils {

    @Test
    void testIndent() {
        assertEquals('    apple', indent('apple'))
        assertEquals('apple', indent('apple', 0))
        assertEquals('        apple', indent('apple', 2))
        assertEquals('    apple\n    orange', indent('apple\norange'))
        assertEquals("${' ' * 24}meow", indent(indent('meow', 5)))
    }

    @Test
    void testSafeEscape() {
        assertEquals('$<$This is a \\$STRING WITH the \'$\\backslash$\' character and others \\%\\#\\{\\}\\slash $>$', safeEscape('<This is a $STRING WITH the \'\\\' character and others %#{}/>'))
    }

    @Test
    void testMaxWordLength() {
        assertEquals(0, maxWordLength(null))
        assertEquals(0, maxWordLength(''))
        assertEquals(5, maxWordLength('hello'))
        assertEquals(5, maxWordLength('hello dude'))
        assertEquals(5, maxWordLength('dude hello'))
        assertEquals(10, maxWordLength('123 12345 1234 123456 1234567890 123 1 1'))
        assertEquals(11, maxWordLength('Urbs antīqua fuit, Tyriī tenuēre colōnī,\nKarthāgō, Ītaliam contrā Tiberīnaque longē'))
    }

    @Test
    void testIsUrl() {
        assertTrue(isUrl('https://xnat.org/testserver'))
        assertTrue(isUrl('http://xnat.org'))
        assertFalse(isUrl('xnat.org'))
        assertFalse(isUrl('Test system'))
    }

    @Test
    void testFormatTestStepDataString() {
        assertEquals('username = admin, password = admin', formatTestStepDataString('username = admin\npassword = admin'))
        assertEquals('\\seqsplit{\\{maximum\\_username\\_length=5000000000\\}}, \\seqsplit{forbidden\\_characters\\_in\\_username=0123456789\\slash}', formatTestStepDataString('{maximum_username_length=5000000000}\nforbidden_characters_in_username=0123456789/'))
        assertEquals('ABCDE FGHIJ KLMNO PQRST UVWXY Z', formatTestStepDataString('ABCDE FGHIJ KLMNO PQRST UVWXY Z')) // seqsplit unused: string is long enough to be considered, but no "word" exceeds threshold
        assertEquals('ABCDEFGHIJKLMNOPQRSTUVWXYZ', formatTestStepDataString('ABCDEFGHIJKLMNOPQRSTUVWXYZ')) // seqsplit unused: a word exceeds the threshold, but the overall string isn't long enough to be an issue
        assertEquals('https:\\slash \\slash notarealurl.notarealwebsite.edu\\#admissions', formatTestStepDataString('https://notarealurl.notarealwebsite.edu#admissions')) // seqsplit unused: forbidden characters: # :
        assertEquals('\\seqsplit{notarealurl.notarealwebsite.edu\\slash admissions}', formatTestStepDataString('notarealurl.notarealwebsite.edu/admissions')) // seqsplit unused: forbidden character
    }

}
