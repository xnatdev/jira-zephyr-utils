package org.nrg.jira.reporter

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.reporter.component.TeXTitlePage
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestTeXTitlePage {

    private static final String PROJECT = 'XNAT'
    private static final String VERSION = '1.7.5.3'
    private static final String JIRA_URL = 'https://xnat.org/jira'
    private static final String BUILD = '1.7.5.3-RC5'

    @Test
    void testTitlePageNoBuildNoEnvironment() {
        assertEquals(FileIOUtils.loadResource('minimal_title_page_example.tex').text, new TeXTitlePage(PROJECT, baseCycle(), VERSION, JIRA_URL, null).generateTeX())
    }

    @Test
    void testTitlePageNoEnvironment() {
        final Cycle cycle = baseCycle()
        cycle.setBuild(BUILD)
        assertEquals(FileIOUtils.loadResource('no_environment_title_example.tex').text, new TeXTitlePage(PROJECT, cycle, VERSION, JIRA_URL, null).generateTeX())
    }

    @Test
    void testTitlePageEnvironmentNonURL() {
        final Cycle cycle = baseCycle()
        cycle.setBuild(BUILD)
        cycle.setEnvironment('xnat-dev05')
        assertEquals(FileIOUtils.loadResource('nonurl_environment_title_example.tex').text, new TeXTitlePage(PROJECT, cycle, VERSION, JIRA_URL, null).generateTeX())
    }

    @Test
    void testTitlePageEnvironmentURL() {
        final Cycle cycle = baseCycle()
        cycle.setBuild(BUILD)
        cycle.setEnvironment('https://xnat.org/xnat-dev05')
        assertEquals(FileIOUtils.loadResource('full_title_example.tex').text, new TeXTitlePage(PROJECT, cycle, VERSION, JIRA_URL, FileIOUtils.loadResource('__files/design.png')).generateTeX())
    }

    private static Cycle baseCycle() {
        new Cycle(name : 'XNAT 1.7.5.3 Regression')
    }

}
