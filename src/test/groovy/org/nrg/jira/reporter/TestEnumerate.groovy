package org.nrg.jira.reporter

import org.nrg.jira.reporter.component.Enumerate
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestEnumerate {

    @Test
    void testSimpleEnumerate() {
        final Enumerate enumerate = new Enumerate('(\\arabic*)')
        enumerate << 'Item number 1'
        enumerate << 'Item number 2'
        assertEquals(FileIOUtils.loadResource('simple_enumerate_example.tex').text, enumerate.generateTeX())
    }

    @Test
    void testNestedEnumerate() {
        final Enumerate innerEnumerate = new Enumerate('(\\alph*)')
        innerEnumerate << 'Item A'
        innerEnumerate << 'Item B'
        final Enumerate outerEnumerate = new Enumerate('(\\arabic*)')
        outerEnumerate << 'Item number 1'
        outerEnumerate << "Item number 2 has a list:\n${innerEnumerate.generateTeX()}".toString()
        assertEquals(FileIOUtils.loadResource('nested_enumerate_example.tex').text, outerEnumerate.generateTeX())
    }

}
