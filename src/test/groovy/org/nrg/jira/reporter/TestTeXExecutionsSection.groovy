package org.nrg.jira.reporter

import org.nrg.jira.BaseMockedZephyrTestCase
import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.reporter.component.TeXExecutionsSection
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import java.nio.file.Files
import java.nio.file.Path

import static org.testng.AssertJUnit.assertEquals

class TestTeXExecutionsSection extends BaseMockedZephyrTestCase {

    @Test
    void testFullExecutionsSection() {
        final Cycle cycle = jiraZephyrController.getCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME)
        final Path tempDirectory = Files.createTempDirectory('tex_test')
        assertEquals(FileIOUtils.loadResource('full_executions_example.tex').text, new TeXExecutionsSection(cycle, jiraZephyrController, tempDirectory, []).generateTeX())
    }

}
