package org.nrg.jira.reporter

import org.nrg.jira.reporter.component.TeXTable
import org.nrg.testing.FileIOUtils
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestTeXTable {

    @Test
    void testGenericTable() {
        final TeXTable genericTable = new TeXTable('| L{3in} | L{3in} |')
        genericTable.addRow('Generic Field1:', 'Generic Value1')
        genericTable.addRow('Generic Field2:', 'Generic Value2')
        assertEquals(FileIOUtils.loadResource('table_1_example.tex').text, genericTable.generateTeX())
    }

}
