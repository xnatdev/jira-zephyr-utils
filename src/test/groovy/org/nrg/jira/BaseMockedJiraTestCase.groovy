package org.nrg.jira

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.core.WireMockConfiguration
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.FileIOUtils
import org.testng.annotations.AfterSuite
import org.testng.annotations.BeforeMethod
import org.testng.annotations.BeforeSuite

import java.nio.file.Files

import static com.github.tomakehurst.wiremock.client.WireMock.*

class BaseMockedJiraTestCase {

    protected static final String VALID_USERNAME = 'USER_001'
    protected static final String VALID_PASSWORD = 'correcthorsebatterystaple'
    protected static final String CONTENT_TYPE = 'Content-Type'
    protected static final String TEXT_HTML = 'text/html'
    protected static final String OCTET_STREAM = 'application/octet-stream'
    protected static final String TEXT_PLAIN = 'text/plain'
    protected static final String MULTIPART = 'multipart/form-data'
    protected static final String MULTIPART_ATLASSIAN_HEADER = 'X-Atlassian-Token'
    protected static final String MULTIPART_ATLASSIAN_HEADER_VALUE = 'nocheck'
    protected static final File TEST_FILE = FileIOUtils.loadResource('__files/design.png')
    protected static final String VERSION_ID = '10000'
    protected static final String VERSION_NAME = '1.7.5.3'
    protected static final String CYCLE_NAME = 'XNAT test suite cycle: 20190207_005826'
    protected static final String CYCLE_ID = '1'
    protected static final String CYCLE_DESCRIPTION = 'Regression tests'
    protected static final String ENVIRONMENT = 'https://xnat.org/testserver'
    protected static final String BUILD = 'Version 1.7.5.3-SNAPSHOT (commit 48)'
    protected static final String PROJECT_KEY = 'XNAT'
    protected static final String PROJECT_ID = '10000'
    protected static final String EXECUTION_1_ID = '20000'
    protected static final String EXECUTION_1_ISSUE_KEY = 'XNAT-100'
    protected static final String EXECUTION_1_ISSUE_ID = '1100'
    protected static final String EXECUTION_1_ISSUE_SUMMARY = 'User Registration Test'
    protected static final String EXECUTION_1_ISSUE_DESCRIPTION = 'Tests registering a new XNAT user.'
    protected static final String EXECUTION_1_STATUS = '2'
    protected static final String EXECUTION_1_ORDER_ID = '2000001'
    protected static final String EXECUTION_2_ID = '20001'
    protected static final String EXECUTION_2_ISSUE_KEY = 'XNAT-101'
    protected static final String EXECUTION_2_ISSUE_ID = '1101'
    protected static final String EXECUTION_2_ISSUE_SUMMARY = 'Project Creation Test'
    protected static final String EXECUTION_2_ISSUE_DESCRIPTION = 'Tests creating a project.'
    protected static final String EXECUTION_2_STATUS = '1'
    protected static final String EXECUTION_2_ORDER_ID = '2000002'
    protected static final String ATTACHMENT_1_NAME = 'screenshot_001.png'
    protected static final String ATTACHMENT_1_ID = '1234567'
    protected static final String ATTACHMENT_2_NAME = 'screenshot_002.png'
    protected static final String ATTACHMENT_2_ID = '1234568'
    protected static final String STEP_1_ID = '50000'
    protected static final String STEP_1_STEP_ID = '500'
    protected static final String STEP_1_COMMENT = 'Pass'
    protected static final String STEP_1_STEP = 'Login to the test system as the admin user.'
    protected static final String STEP_1_DATA = 'Basic auth admin:admin'
    protected static final String STEP_1_RESULT = 'Login is successful.'
    protected static final String STEP_1_STATUS = '1'
    protected static final String STEP_2_ID = '50001'
    protected static final String STEP_2_STEP_ID = '501'
    protected static final String STEP_2_COMMENT = 'User creation returned 500 error'
    protected static final String STEP_2_STEP = 'Create a new user.'
    protected static final String STEP_2_DATA = 'username = testuser_0001'
    protected static final String STEP_2_RESULT = 'Specified user account is created.'
    protected static final String STEP_2_STATUS = '2'
    private static final   Map<String, String> REPLACEMENTS = replacements()

    protected static final String VERSIONS_JSON = resourceJson('versions')
    protected static final String PROJECT_JSON = resourceJson('project').replace('%VERSIONS%', VERSIONS_JSON)
    protected static final String ISSUE_JSON = resourceJson('issue')
    protected static final String CYCLES_JSON = resourceJson('cycles')
    protected static final String CYCLE_EXECUTIONS_JSON = resourceJson('cycle_executions')
    protected static final String CYCLE_JSON = resourceJson('cycle')
    protected static final String ATTACHMENTS_JSON = resourceJson('attachments')
    protected static final String STEPS_FROM_ISSUE_JSON = resourceJson('steps_from_issue')
    protected static final String STEPS_FROM_EXECUTION_JSON = resourceJson('steps_from_execution')
    protected static final String CYCLE_CREATE_RESPONSE_JSON = resourceJson('cycle_create')
    protected static final String CYCLE_UPDATE_JSON = resourceJson('cycle_update')
    protected static final String EXECUTION_ADDED_JSON = resourceJson('execution_added')

    protected static WireMockServer mockServer
    protected static String mockUrl

    @BeforeSuite
    void setupMockHttpServer() {
        getMockUrlOrSetup()
    }

    @BeforeMethod(alwaysRun = true)
    void resetStubs() {
        mockServer.resetAll()
        stubFor(get(urlPathEqualTo('/rest/zapi/latest/cycle')).willReturn(aResponse().withStatus(400).withHeader(CONTENT_TYPE, TEXT_HTML)))
        stubFor(get("/rest/api/2/project/${PROJECT_KEY}/versions").willReturn(okJson(VERSIONS_JSON)))
        stubFor(get("/rest/api/2/project/${PROJECT_KEY}").willReturn(okJson(PROJECT_JSON)))
        stubFor(get("/rest/api/latest/issue/${EXECUTION_1_ISSUE_KEY}").willReturn(okJson(ISSUE_JSON)))
        addIssueStub(EXECUTION_1_ISSUE_KEY, EXECUTION_1_ISSUE_ID, EXECUTION_1_ISSUE_SUMMARY, EXECUTION_1_ISSUE_DESCRIPTION)
        addIssueStub(EXECUTION_2_ISSUE_KEY, EXECUTION_2_ISSUE_ID, EXECUTION_2_ISSUE_SUMMARY, EXECUTION_2_ISSUE_DESCRIPTION)
    }

    @AfterSuite(alwaysRun = true)
    void stopMockHttpServer() {
        mockServer.stop()
    }

    protected static String getMockUrlOrSetup() {
        if (mockUrl != null) {
            mockUrl
        } else {
            mockServer = new WireMockServer(WireMockConfiguration.wireMockConfig().dynamicPort())
            mockServer.start()
            configureFor('localhost', mockServer.port())
            mockUrl = mockServer.baseUrl()
        }
    }

    protected void addJiraCheckStub(boolean jiraValid) {
        stubFor(get('/rest/api/2/dashboard').willReturn(aResponse().withStatus(jiraValid ? 200 : 404).withHeader(CONTENT_TYPE, TEXT_HTML)))
    }

    protected void addAuthCheckStub() {
        addJiraCheckStub(true)
        stubFor(get('/rest/api/2/myself').willReturn(unauthorized()))
        stubFor(get('/rest/api/2/myself').withBasicAuth(VALID_USERNAME, VALID_PASSWORD).willReturn(aResponse().withStatus(200).withHeader(CONTENT_TYPE, TEXT_HTML)))
    }
    
    private void addIssueStub(String issueKey, String issueId, String issueSummary, String issueDescription) {
        final String jsonBody = CommonStringUtils.replaceEach(ISSUE_JSON, [
                '%ISSUE_KEY%' : issueKey,
                '%ISSUE_ID%' : issueId,
                '%ISSUE_SUMMARY%' : issueSummary,
                '%ISSUE_DESCRIPTION%' : issueDescription
        ])
        stubFor(get("/rest/api/2/issue/${issueKey}").willReturn(okJson(jsonBody)))
        stubFor(get("/rest/api/2/issue/${issueId}").willReturn(okJson(jsonBody)))
    }

    protected void addFullZephyrMock() { // except for things we want to check failing
        stubFor(get("/rest/zapi/latest/cycle?versionId=${VERSION_ID}").willReturn(okJson(CYCLES_JSON)))
        stubFor(get("/rest/zapi/latest/execution?cycleId=${CYCLE_ID}").willReturn(okJson(CYCLE_EXECUTIONS_JSON)))
        stubFor(get("/rest/zapi/latest/cycle/${CYCLE_ID}").willReturn(okJson(CYCLE_JSON)))
        addAttachmentStub(EXECUTION_1_ID, 'execution')
        addAttachmentStub(EXECUTION_2_ID, 'execution')
        addAttachmentStub(STEP_1_ID, 'TESTSTEPRESULT')
        addAttachmentStub(STEP_2_ID, 'TESTSTEPRESULT')
        stubFor(get("/rest/zapi/latest/teststep/${EXECUTION_1_ISSUE_ID}").willReturn(okJson(STEPS_FROM_ISSUE_JSON)))
        stubFor(get("/rest/zapi/latest/teststep/${EXECUTION_2_ISSUE_ID}").willReturn(okJson(STEPS_FROM_ISSUE_JSON)))
        stubFor(get("/rest/zapi/latest/stepResult?executionId=${EXECUTION_1_ID}").willReturn(okJson(STEPS_FROM_EXECUTION_JSON)))
        stubFor(get("/rest/zapi/latest/stepResult?executionId=${EXECUTION_2_ID}").willReturn(okJson(STEPS_FROM_EXECUTION_JSON)))
        stubFor(get(urlPathEqualTo('/plugins/servlet/schedule/viewAttachment')).withQueryParams([id : equalTo(ATTACHMENT_1_ID), name : equalTo(ATTACHMENT_1_NAME)]).willReturn(aResponse().withStatus(200).withHeader(CONTENT_TYPE, OCTET_STREAM).withBodyFile(TEST_FILE.name)))
        stubFor(get(urlPathEqualTo('/plugins/servlet/schedule/viewAttachment')).withQueryParams([id : equalTo(ATTACHMENT_2_ID), name : equalTo(ATTACHMENT_2_NAME)]).willReturn(aResponse().withStatus(200).withHeader(CONTENT_TYPE, OCTET_STREAM).withBodyFile(TEST_FILE.name)))
        stubFor(post(urlPathEqualTo('/rest/zapi/latest/cycle')).willReturn(okJson(CYCLE_CREATE_RESPONSE_JSON)))
        stubFor(put(urlPathEqualTo('/rest/zapi/latest/cycle')).willReturn(okJson(CYCLE_UPDATE_JSON)))
        addExecutionAddStub(EXECUTION_1_ISSUE_ID, EXECUTION_1_ID, EXECUTION_1_ORDER_ID, EXECUTION_1_ISSUE_KEY, EXECUTION_1_ISSUE_SUMMARY)
        addExecutionAddStub(EXECUTION_2_ISSUE_ID, EXECUTION_2_ID, EXECUTION_2_ORDER_ID, EXECUTION_2_ISSUE_KEY, EXECUTION_2_ISSUE_SUMMARY)
        stubFor(put("/rest/zapi/latest/execution/${EXECUTION_1_ID}/execute").willReturn(okForContentType(TEXT_PLAIN, 'Response body not modeled')))
        addAttachmentPostStub(EXECUTION_1_ID, 'execution')
        addAttachmentPostStub(STEP_1_ID, 'stepresult')
        stubFor(put("/rest/zapi/latest/stepResult/${STEP_1_ID}").willReturn(okForContentType(TEXT_PLAIN, 'Response body not modeled')))
    }

    protected void addZapiCheckStub(boolean zapiInstalled) {
        addAuthCheckStub()
        stubFor(get('/rest/zapi/latest/license').willReturn(aResponse().withStatus(zapiInstalled ? 200 : 404).withHeader(CONTENT_TYPE, TEXT_HTML)))
    }

    private void addAttachmentStub(String entityId, String entityType) {
        stubFor(get(urlPathEqualTo('/rest/zapi/latest/attachment/attachmentsByEntity')).withQueryParams([entityId : equalTo(entityId), entityType : equalTo(entityType)]).willReturn(okJson(ATTACHMENTS_JSON)))
    }

    private void addExecutionAddStub(String issueId, String executionId, String orderId, String issueKey, String issueSummary) {
        final Map<String, String> replacements = [
                '%EXECUTION_ID%' : executionId,
                '%EXECUTION_ORDER_ID%' : orderId,
                '%ISSUE_ID%' : issueId,
                '%ISSUE_KEY%' : issueKey,
                '%ISSUE_SUMMARY%' : issueSummary
        ]
        stubFor(post('/rest/zapi/latest/execution').withRequestBody(matchingJsonPath("\$[?(@.issueId == ${issueId})]")).willReturn(okJson(CommonStringUtils.replaceEach(EXECUTION_ADDED_JSON, replacements))))
    }

    private void addAttachmentPostStub(String entityId, String entityType) {
        stubFor(post(urlPathEqualTo('/rest/zapi/latest/attachment')).
                withQueryParams([entityId : equalTo(entityId), entityType : equalTo(entityType)]).
                withHeader(MULTIPART_ATLASSIAN_HEADER, equalTo(MULTIPART_ATLASSIAN_HEADER_VALUE)).
                withHeader(CONTENT_TYPE, containing(MULTIPART)).
                withMultipartRequestBody(aMultipart().withBody(binaryEqualTo(Files.readAllBytes(TEST_FILE.toPath())))).
                willReturn(okForContentType(TEXT_PLAIN, ''))
        )
    }

    private static Map<String, String> replacements() {
        [
                '%VERSION_ID%' : VERSION_ID,
                '%VERSION_NAME%' : VERSION_NAME,
                '%CYCLE_NAME%' : CYCLE_NAME,
                '%CYCLE_ID%' : CYCLE_ID,
                '%CYCLE_DESCRIPTION%' : CYCLE_DESCRIPTION,
                '%ENVIRONMENT%' : ENVIRONMENT,
                '%BUILD%' : BUILD,
                '%PROJECT_KEY%' : PROJECT_KEY,
                '%PROJECT_ID%' : PROJECT_ID,
                '%EXECUTION_1_ID%' : EXECUTION_1_ID,
                '%EXECUTION_1_ISSUE_KEY%' : EXECUTION_1_ISSUE_KEY,
                '%EXECUTION_1_ISSUE_ID%' : EXECUTION_1_ISSUE_ID, 
                '%EXECUTION_1_ISSUE_SUMMARY%' : EXECUTION_1_ISSUE_SUMMARY,
                '%EXECUTION_1_ISSUE_DESCRIPTION%' : EXECUTION_1_ISSUE_DESCRIPTION,
                '%EXECUTION_1_STATUS%' : EXECUTION_1_STATUS,
                '%EXECUTION_1_ORDER_ID%' : EXECUTION_1_ORDER_ID,
                '%EXECUTION_2_ID%' : EXECUTION_2_ID,
                '%EXECUTION_2_ISSUE_KEY%' : EXECUTION_2_ISSUE_KEY,
                '%EXECUTION_2_ISSUE_ID%' : EXECUTION_2_ISSUE_ID,
                '%EXECUTION_2_ISSUE_SUMMARY%' : EXECUTION_2_ISSUE_SUMMARY,
                '%EXECUTION_2_ISSUE_DESCRIPTION%' : EXECUTION_2_ISSUE_DESCRIPTION,
                '%EXECUTION_2_STATUS%' : EXECUTION_2_STATUS,
                '%EXECUTION_2_ORDER_ID%' : EXECUTION_2_ORDER_ID,
                '%ATTACHMENT_1_NAME%' : ATTACHMENT_1_NAME,
                '%ATTACHMENT_1_ID%' : ATTACHMENT_1_ID,
                '%ATTACHMENT_2_NAME%' : ATTACHMENT_2_NAME,
                '%ATTACHMENT_2_ID%' : ATTACHMENT_2_ID,
                '%STEP_1_ID%' : STEP_1_ID,
                '%STEP_1_STEP_ID%' : STEP_1_STEP_ID,
                '%STEP_1_COMMENT%' : STEP_1_COMMENT,
                '%STEP_1_STEP%' : STEP_1_STEP,
                '%STEP_1_DATA%' : STEP_1_DATA,
                '%STEP_1_RESULT%' : STEP_1_RESULT,
                '%STEP_1_STATUS%' : STEP_1_STATUS,
                '%STEP_2_ID%' : STEP_2_ID,
                '%STEP_2_STEP_ID%' : STEP_2_STEP_ID,
                '%STEP_2_COMMENT%' : STEP_2_COMMENT,
                '%STEP_2_STEP%' : STEP_2_STEP,
                '%STEP_2_DATA%' : STEP_2_DATA,
                '%STEP_2_RESULT%' : STEP_2_RESULT,
                '%STEP_2_STATUS%' : STEP_2_STATUS
        ]
    }

    protected static String resourceJson(String fileName) {
        CommonStringUtils.replaceEach(FileIOUtils.loadResource("${fileName}.json").text, REPLACEMENTS)
    }

}
