package org.nrg.jira

import org.testng.annotations.BeforeMethod

class BaseMockedZephyrTestCase extends BaseMockedJiraTestCase {

    protected final JiraZephyrController jiraZephyrController = new JiraZephyrController(getMockUrlOrSetup(), VALID_USERNAME, VALID_PASSWORD, false)

    @BeforeMethod(alwaysRun = true)
    void addZapiStubs() {
        addFullZephyrMock()
    }

}
