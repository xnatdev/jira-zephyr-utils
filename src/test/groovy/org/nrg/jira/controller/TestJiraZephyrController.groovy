package org.nrg.jira.controller

import org.nrg.jira.BaseMockedZephyrTestCase
import org.nrg.jira.JiraZephyrController
import org.nrg.jira.components.Attachment
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.jira.components.zephyr.TestStep
import org.nrg.jira.exceptions.AttachmentNotFound
import org.nrg.jira.exceptions.CycleNotFoundException
import org.nrg.jira.exceptions.JiraIssueNotFoundException
import org.nrg.jira.exceptions.LocalFileNotFoundException
import org.nrg.jira.exceptions.ProjectNotFoundException
import org.nrg.jira.exceptions.VersionNotFoundException
import org.nrg.jira.exceptions.ZapiNotFoundException
import org.nrg.jira.components.zephyr.Cycle
import org.nrg.testing.TestNgUtils
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import java.nio.file.Files

import static com.github.tomakehurst.wiremock.client.WireMock.*
import static org.testng.AssertJUnit.assertEquals

class TestJiraZephyrController extends BaseMockedZephyrTestCase {

    private Execution sampleExecution
    private TestStep sampleStep

    @BeforeMethod(alwaysRun = true)
    void formSampleObjects() {
        sampleStep = new TestStep(id : STEP_1_ID as int, stepId : STEP_1_STEP_ID as int)
        sampleExecution = new Execution(id : EXECUTION_1_ID as int)
    }

    @Test(expectedExceptions = ZapiNotFoundException)
    void testZapiNotInstalled() {
        addZapiCheckStub(false)
        new JiraZephyrController(mockUrl, VALID_USERNAME, VALID_PASSWORD)
    }

    @Test
    void testSuccessfulJiraZephyrControllerVerification() {
        addZapiCheckStub(true)
        new JiraZephyrController(mockUrl, VALID_USERNAME, VALID_PASSWORD)
    }

    @Test(expectedExceptions = CycleNotFoundException)
    void testReadCycleWrongName() {
        jiraZephyrController.getCycle('Incorrect', PROJECT_KEY, VERSION_NAME)
    }

    @Test(expectedExceptions = ProjectNotFoundException)
    void testReadCycleWrongProjectKey() {
        jiraZephyrController.getCycle(CYCLE_NAME, 'Incorrect', VERSION_NAME)
    }

    @Test(expectedExceptions = VersionNotFoundException)
    void testReadCycleWrongVersionName() {
        jiraZephyrController.getCycle(CYCLE_NAME, PROJECT_KEY, 'Incorrect')
    }

    @Test
    void testReadCycleByNames() {
        assertCycleCorrect(jiraZephyrController.getCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME))
    }

    @Test(expectedExceptions = CycleNotFoundException)
    void testReadCycleWrongId() {
        jiraZephyrController.getCycle('Incorrect')
    }

    @Test
    void testReadCycleById() {
        assertCycleCorrect(jiraZephyrController.getCycle(CYCLE_ID))
    }

    @Test(expectedExceptions = AttachmentNotFound)
    void testDownloadAttachmentWrongId() {
        jiraZephyrController.downloadAttachment(new Attachment(fileName : ATTACHMENT_1_NAME, fileId : 10), null)
    }

    @Test
    void testDownloadAttachment() {
        final File tempFile = Files.createTempFile('testfile', 'png').toFile()
        jiraZephyrController.downloadAttachment(new Attachment(fileName : ATTACHMENT_1_NAME, fileId : ATTACHMENT_1_ID as int), tempFile)
        TestNgUtils.assertBinaryFilesEqual(TEST_FILE, tempFile)
    }

    @Test(expectedExceptions = ProjectNotFoundException)
    void testCreateCycleWrongProject() {
        jiraZephyrController.createCycle(CYCLE_NAME, 'Incorrect', VERSION_NAME)
    }

    @Test(expectedExceptions = VersionNotFoundException)
    void testCreateCycleWrongVersion() {
        jiraZephyrController.createCycle(CYCLE_NAME, PROJECT_KEY, 'Incorrect')
    }

    @Test
    void testCreateCycle() {
        assertCycleCorrect(jiraZephyrController.createCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME)) // this is going to check a bunch of executions and all that which we haven't "created", but that's fine
    }

    @Test
    void testUpdateBuildInfo() {
        final String newBuild = 'XNAT-TEST-BUILD'
        final Cycle cycle = jiraZephyrController.createCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME)
        jiraZephyrController.updateBuildInfo(cycle, newBuild)
        assertEquals(newBuild, cycle.build)
    }

    @Test
    void testUpdateEnvironmentInfo() {
        final String newEnvironment = 'https://xnat.org/othertestserver'
        final Cycle cycle = jiraZephyrController.createCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME)
        jiraZephyrController.updateEnvironmentInfo(cycle, newEnvironment)
        assertEquals(newEnvironment, cycle.environment)
    }

    @Test(expectedExceptions = JiraIssueNotFoundException)
    void testAddTestsToCycleWrongTestKey() {
        jiraZephyrController.addTestsToCycle(null, ['INCORR-101'])
    }

    @Test
    void testAddTestsToCycle() {
        final Cycle cycle = jiraZephyrController.createCycle(CYCLE_NAME, PROJECT_KEY, VERSION_NAME)
        cycle.executions.clear()
        assertEquals(0, cycle.executions.size())
        jiraZephyrController.addTestsToCycle(cycle, [EXECUTION_1_ISSUE_KEY, EXECUTION_2_ISSUE_KEY])
        assertEquals(2, cycle.executions.size())
        assertExecution(cycle.executions[0], EXECUTION_1_ID, EXECUTION_1_ISSUE_ID, EXECUTION_1_ISSUE_KEY, TestStatus.UNEXECUTED.jiraValue as String, EXECUTION_1_ISSUE_SUMMARY, false)
        assertExecution(cycle.executions[1], EXECUTION_2_ID, EXECUTION_2_ISSUE_ID, EXECUTION_2_ISSUE_KEY, TestStatus.UNEXECUTED.jiraValue as String, EXECUTION_2_ISSUE_SUMMARY, false)
    }

    @Test
    void testUpdateExecutionStatus() {
        jiraZephyrController.updateExecutionStatus(sampleExecution, TestStatus.WIP)
        assertEquals(TestStatus.WIP, sampleExecution.status)
        jiraZephyrController.updateExecutionStatus(sampleExecution, TestStatus.FAIL)
        assertEquals(TestStatus.FAIL, sampleExecution.status)
    }

    @Test
    void testAddExecutionComment() {
        jiraZephyrController.postExecutionComment(sampleExecution, 'Testing')
        assertEquals('Testing', sampleExecution.comment)
        jiraZephyrController.postExecutionComment(sampleExecution, '12345' * 1000)
        assertEquals('12345' * 100 + '...', sampleExecution.comment)
    }

    @Test
    void testPostDefect() {
        jiraZephyrController.postDefect(sampleExecution, EXECUTION_1_ISSUE_KEY) // not much to test here since: 1. defect isn't modeled. 2. defect is by key and not id
    }

    @Test(expectedExceptions = LocalFileNotFoundException)
    void testExecutionAttachmentUploadNullFile() {
        jiraZephyrController.postExecutionAttachment(sampleExecution, null)
    }

    @Test(expectedExceptions = LocalFileNotFoundException)
    void testExecutionAttachmentUploadMissingFile() {
        jiraZephyrController.postExecutionAttachment(sampleExecution, new File('doesnotexist.txt'))
    }

    @Test
    void testExecutionAttachmentUpload() {
        assertEquals(0, sampleExecution.executionAttachments.size())
        2.times { // want to check that attachment isn't added to list twice
            jiraZephyrController.postExecutionAttachment(sampleExecution, TEST_FILE)
            assertEquals(1, sampleExecution.executionAttachments.size())
            assertEquals(TEST_FILE.name, sampleExecution.executionAttachments[0].fileName)
        }
    }

    @Test(expectedExceptions = LocalFileNotFoundException)
    void testStepAttachmentUploadNullFile() {
        jiraZephyrController.postStepAttachment(sampleStep, null)
    }

    @Test(expectedExceptions = LocalFileNotFoundException)
    void testStepAttachmentUploadMissingFile() {
        jiraZephyrController.postStepAttachment(sampleStep, new File('doesnotexist.txt'))
    }

    @Test
    void testStepAttachmentUpload() {
        assertEquals(0, sampleStep.stepAttachments.size())
        2.times { // want to check that attachment isn't added to list twice
            jiraZephyrController.postStepAttachment(sampleStep, TEST_FILE)
            assertEquals(1, sampleStep.stepAttachments.size())
            assertEquals(TEST_FILE.name, sampleStep.stepAttachments[0].fileName)
        }
    }

    @Test
    void testUpdateStepResult() {
        jiraZephyrController.updateStepResult(sampleStep, TestStatus.PASS, '100')
        assertEquals(TestStatus.PASS, sampleStep.stepStatus)
        assertEquals('100', sampleStep.comment)
        jiraZephyrController.updateStepResult(sampleStep, TestStatus.FAIL, null)
        assertEquals(TestStatus.FAIL, sampleStep.stepStatus)
        assertEquals('100', sampleStep.comment)
        jiraZephyrController.updateStepResult(sampleStep, null, '1234567890' * 10000)
        assertEquals(TestStatus.FAIL, sampleStep.stepStatus)
        assertEquals('1234567890' * 50 + '...', sampleStep.comment)
    }

    private void assertCycleCorrect(Cycle cycle) {
        assertEquals(CYCLE_ID, cycle.id)
        assertEquals(CYCLE_NAME, cycle.name)
        assertEquals(ENVIRONMENT, cycle.environment)
        assertEquals(BUILD, cycle.build)
        assertEquals(CYCLE_DESCRIPTION, cycle.description)
        assertEquals(PROJECT_ID, cycle.projectId)
        assertEquals(VERSION_ID, cycle.versionId)
        assertEquals(2, cycle.executions.size())
        assertExecution(cycle.executions[0], EXECUTION_1_ID, EXECUTION_1_ISSUE_ID, EXECUTION_1_ISSUE_KEY, EXECUTION_1_STATUS, EXECUTION_1_ISSUE_SUMMARY)
        assertExecution(cycle.executions[1], EXECUTION_2_ID, EXECUTION_2_ISSUE_ID, EXECUTION_2_ISSUE_KEY, EXECUTION_2_STATUS, EXECUTION_2_ISSUE_SUMMARY)
    }

    private void assertExecution(Execution execution, String executionId, String issueId, String issueKey, String statusString, String summary, boolean checkAttachmentsAndSteps = true) {
        assertEquals(executionId as int, execution.id)
        assertEquals(issueId, execution.issueId)
        assertEquals(issueKey, execution.issueKey)
        assertEquals(TestStatus.findByValue(statusString as int), execution.status)
        assertEquals(summary, execution.summary)
        if (checkAttachmentsAndSteps) {
            assertAttachments(execution.executionAttachments)
            assertEquals(2, execution.testSteps.size())
            assertStep(execution.testSteps[0], STEP_1_ID, STEP_1_STEP_ID, STEP_1_STEP, STEP_1_DATA, STEP_1_RESULT, STEP_1_COMMENT, STEP_1_STATUS)
            assertStep(execution.testSteps[1], STEP_2_ID, STEP_2_STEP_ID, STEP_2_STEP, STEP_2_DATA, STEP_2_RESULT, STEP_2_COMMENT, STEP_2_STATUS)
        }
    }

    private void assertAttachments(List<Attachment> attachments) {
        assertEquals(2, attachments.size())
        assertEquals(ATTACHMENT_1_ID as int, attachments[0].fileId)
        assertEquals(ATTACHMENT_1_NAME, attachments[0].fileName)
        assertEquals(ATTACHMENT_2_ID as int, attachments[1].fileId)
        assertEquals(ATTACHMENT_2_NAME, attachments[1].fileName)
    }

    private void assertStep(TestStep testStep, String id, String stepId, String step, String data, String result, String comment, String status) {
        assertEquals(id as int, testStep.id)
        assertEquals(stepId as int, testStep.stepId)
        assertEquals(step, testStep.step)
        assertEquals(data, testStep.data)
        assertEquals(result, testStep.result)
        assertEquals(comment, testStep.comment)
        assertEquals(TestStatus.findByValue(status as int), testStep.stepStatus)
        assertAttachments(testStep.stepAttachments)
    }

}
