package org.nrg.jira.controller

import org.nrg.jira.BaseMockedJiraTestCase
import org.nrg.jira.JiraController
import org.nrg.jira.exceptions.*
import org.testng.annotations.*

import static org.testng.AssertJUnit.assertEquals

class TestJiraController extends BaseMockedJiraTestCase {

    private JiraController jiraController

    @BeforeClass
    void setupController() {
        jiraController = new JiraController(mockUrl, VALID_USERNAME, VALID_PASSWORD, false)
    }

    @Test(expectedExceptions = JiraConnectionException)
    void testBadConnection() {
        new JiraController(mockUrl.replace('http', 'https'), VALID_USERNAME, VALID_PASSWORD)
    }

    @Test(expectedExceptions = JiraNotFoundException)
    void testServerIsntJiraInstance() {
        addJiraCheckStub(false)
        new JiraController(mockUrl, VALID_USERNAME, VALID_PASSWORD)
    }

    @Test(expectedExceptions = InvalidCredentialsException)
    void testInvalidAuth() {
        addAuthCheckStub()
        new JiraController(mockUrl, VALID_USERNAME, 'h4x0rzed')
    }

    @Test
    void testSuccessfulJiraControllerVerification() {
        addAuthCheckStub()
        new JiraController(mockUrl, VALID_USERNAME, VALID_PASSWORD)
    }

    @Test(expectedExceptions = ProjectNotFoundException)
    void testReadProjectIdWrongProject() {
        jiraController.getProjectId('Incorrect')
    }

    @Test
    void testReadProjectId() {
        assertEquals(PROJECT_ID, jiraController.getProjectId(PROJECT_KEY))
    }

    @Test(expectedExceptions = ProjectNotFoundException)
    void testReadVersionWrongProject() {
        jiraController.getVersionId('Incorrect', VERSION_NAME)
    }

    @Test(expectedExceptions = VersionNotFoundException)
    void testReadVersionWrongVersion() {
        jiraController.getVersionId(PROJECT_KEY, 'Incorrect')
    }

    @Test
    void testReadVersionId() {
        assertEquals(VERSION_ID, jiraController.getVersionId(PROJECT_KEY, VERSION_NAME))
    }

    @Test(expectedExceptions = JiraIssueNotFoundException)
    void testIssueIdWrongIssueKey() {
        jiraController.getIssueId('INCOR-1')
    }

    @Test
    void testReadIssueId() {
        assertEquals(EXECUTION_1_ISSUE_ID, jiraController.getIssueId(EXECUTION_1_ISSUE_KEY))
    }

}
