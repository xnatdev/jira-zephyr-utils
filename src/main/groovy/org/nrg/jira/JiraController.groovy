package org.nrg.jira

import com.jayway.restassured.path.json.JsonPath
import com.jayway.restassured.response.Response
import com.jayway.restassured.specification.RequestSpecification
import org.nrg.jira.exceptions.*
import org.nrg.jira.components.JiraIssue
import org.nrg.testing.CommonStringUtils

import static com.jayway.restassured.RestAssured.*

@SuppressWarnings('unused')
class JiraController {

    private final String jiraUrl, username, password

    JiraController(String jiraUrl, String username, String password, boolean verifyCredentials = true) throws JiraZephyrException {
        this.jiraUrl = jiraUrl
        this.username = username
        this.password = password

        if (!verifyCredentials) return

        try {
            final int jiraResponseCode = get(formatJiraUrl('/rest/api/2/dashboard')).statusCode
            if (jiraResponseCode != 200) {
                throw new JiraNotFoundException(jiraResponseCode)
            }
        } catch (Exception e) {
            if (e instanceof JiraNotFoundException) {
                throw e
            } else {
                throw new JiraConnectionException(e)
            }
        }

        final int authResponseCode = getCredentials().get(formatJiraUrl('/rest/api/2/myself')).statusCode
        if (authResponseCode != 200) {
            throw new InvalidCredentialsException(authResponseCode)
        }
    }

    protected RequestSpecification getCredentials() {
        given().authentication().preemptive().basic(username, password)
    }

    String getProjectId(String projectName) throws JiraZephyrException {
        final Response projectResponse = getCredentials().get(formatJiraUrl('/rest/api/2/project', projectName))
        if (projectResponse.statusCode == 200) {
            projectResponse.path('id')
        } else {
            throw new ProjectNotFoundException(projectResponse.statusCode)
        }
    }

    String getVersionId(String projectName, String versionName) throws JiraZephyrException {
        final Response versionResponse = getCredentials().get(formatJiraUrl('/rest/api/2/project', projectName, 'versions'))
        if (versionResponse.statusCode != 200) {
            throw new ProjectNotFoundException(versionResponse.statusCode)
        } else {
            final Map<String, String> foundVersion = versionResponse.path("find {it.name == '${versionName}'}") as Map<String, String>
            if (foundVersion) {
                foundVersion['id']
            } else {
                throw new VersionNotFoundException(versionResponse.statusCode)
            }
        }
    }

    JiraIssue getIssue(String issueKey) throws JiraZephyrException {
        final Response response = getCredentials().get(formatJiraUrl('/rest/api/2/issue', issueKey))
        if (response.statusCode == 200) {
            final JsonPath jsonPath = response.jsonPath()
            final JiraIssue issue = jsonPath.getObject('fields', JiraIssue)
            issue.setId(jsonPath.getString('id'))
            issue
        } else {
            throw new JiraIssueNotFoundException(response.statusCode)
        }
    }

    String getIssueId(String jiraIssueKey) throws JiraZephyrException {
        getIssue(jiraIssueKey).id
    }

    String getProjectLink(String project) {
        formatJiraUrl('projects', project)
    }

    String formatJiraUrl(Object... paths) {
        CommonStringUtils.formatUrl(jiraUrl, CommonStringUtils.formatUrl(paths))
    }

}
