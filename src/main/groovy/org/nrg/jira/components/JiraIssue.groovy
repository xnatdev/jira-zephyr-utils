package org.nrg.jira.components

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class JiraIssue {

    String id
    String summary
    String description

}
