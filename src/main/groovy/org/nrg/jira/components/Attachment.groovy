package org.nrg.jira.components

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
class Attachment implements Comparable<Attachment> {

    String fileName
    int fileId

    @Override
    int compareTo(Attachment other) {
        this.fileId - other.fileId
    }

}
