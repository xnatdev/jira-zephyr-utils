package org.nrg.jira.components.zephyr

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.nrg.testing.CommonStringUtils

@SuppressWarnings('unused')
@JsonIgnoreProperties(ignoreUnknown = true)
class Cycle {

    String createdByDisplay
    String build
    int totalExecutions
    String environment
    String description
    String name
    String id
    String projectId
    String versionId
    List<Execution> executions = []

    void addExecution(Execution execution) {
        executions << execution
    }

    String getCycleUrl(String baseJiraUrl, String projectName) {
        final String query = "project=\"${projectName}\" AND cycleName=\"${name}\"".replaceAll(' ', '%20').replaceAll('"', '%22').replaceAll(':', '%3A').replaceAll('=', '%3D')
        CommonStringUtils.formatUrl(baseJiraUrl, "secure/enav/#?query=${query}")
    }

}
