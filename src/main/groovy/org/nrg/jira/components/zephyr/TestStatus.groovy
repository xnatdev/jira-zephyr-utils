package org.nrg.jira.components.zephyr

enum TestStatus {

    UNEXECUTED (-1, 'UNEXECUTED'),
    PASS       ( 1, 'PASS'),
    FAIL       ( 2, 'FAIL'),
    WIP        ( 3, 'IN PROGRESS'),
    BLOCKED    ( 4, 'BLOCKED')

    private final int jiraValue
    private final String displayName

    TestStatus(int jiraValue, String displayName) {
        this.jiraValue = jiraValue
        this.displayName = displayName
    }

    int getJiraValue() {
        jiraValue
    }

    String getDisplayName() {
        displayName
    }

    static TestStatus findByValue(int value) {
        values().find { status ->
            status.jiraValue == value
        }
    }

}
