package org.nrg.jira.components.zephyr

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.nrg.jira.components.Attachment

@SuppressWarnings('unused')
@JsonIgnoreProperties(ignoreUnknown = true)
class Execution {

    int id
    String issueId
    String testDescription
    int executionStatus
    TestStatus status
    String issueKey
    String summary
    String executedByDisplay
    String executedOn
    String comment
    int orderId
    List<Attachment> executionAttachments = []
    List<TestStep> testSteps = []

    void setExecutionStatus(int executionStatus) {
        this.executionStatus = executionStatus
        setStatus(TestStatus.findByValue(executionStatus))
    }

}
