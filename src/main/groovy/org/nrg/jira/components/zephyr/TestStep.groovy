package org.nrg.jira.components.zephyr

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import org.nrg.jira.components.Attachment

@SuppressWarnings('unused')
@JsonIgnoreProperties(ignoreUnknown = true)
class TestStep {

    int id
    int stepId
    int orderId
    String step
    String data
    String result
    String comment
    TestStatus stepStatus = TestStatus.UNEXECUTED
    List<Attachment> stepAttachments = []

    void setStatus(int status) {
        setStepStatus(TestStatus.findByValue(status))
    }

}
