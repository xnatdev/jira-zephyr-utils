package org.nrg.jira.exceptions

class LocalFileNotFoundException extends JiraZephyrException {

    LocalFileNotFoundException() {
        super('Specified file to upload as attachment did not exist [or was null]', null)
    }

}
