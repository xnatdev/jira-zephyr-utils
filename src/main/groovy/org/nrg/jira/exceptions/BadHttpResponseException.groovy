package org.nrg.jira.exceptions

class BadHttpResponseException extends JiraZephyrException {

    BadHttpResponseException(String explanation, int statusCode) {
        super(explanation, statusCode)
    }

}
