package org.nrg.jira.exceptions

class JiraNotFoundException extends JiraZephyrException {

    JiraNotFoundException(int statusCode) {
        super('Could not locate a valid JIRA server at the provided URL', statusCode)
    }

}
