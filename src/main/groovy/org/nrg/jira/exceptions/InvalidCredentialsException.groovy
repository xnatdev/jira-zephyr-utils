package org.nrg.jira.exceptions

class InvalidCredentialsException extends JiraZephyrException {

    InvalidCredentialsException(int statusCode) {
        super('Authentication to the JIRA server failed', statusCode)
    }

}
