package org.nrg.jira.exceptions

class CycleNotFoundException extends JiraZephyrException {

    CycleNotFoundException(int statusCode) {
        super('Could not find requested cycle', statusCode)
    }

}
