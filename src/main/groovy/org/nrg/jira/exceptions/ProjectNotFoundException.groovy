package org.nrg.jira.exceptions

class ProjectNotFoundException extends JiraZephyrException {

    ProjectNotFoundException(int statusCode) {
        super('Could not find requested JIRA project', statusCode)
    }

}
