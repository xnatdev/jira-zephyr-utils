package org.nrg.jira.exceptions

class EntityNotFoundException extends JiraZephyrException{

    EntityNotFoundException(int statusCode) {
        super('Could not find attachments for entity', statusCode)
    }

}
