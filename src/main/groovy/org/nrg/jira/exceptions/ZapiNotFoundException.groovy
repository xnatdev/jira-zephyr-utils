package org.nrg.jira.exceptions

class ZapiNotFoundException extends JiraZephyrException {

    ZapiNotFoundException(int statusCode) {
        super('ZAPI, the API for interacting with Zephyr, does not seem to be installed on the JIRA server', statusCode)
    }

}
