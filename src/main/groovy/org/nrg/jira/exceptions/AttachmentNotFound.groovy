package org.nrg.jira.exceptions

class AttachmentNotFound extends JiraZephyrException {

    AttachmentNotFound(int statusCode) {
        super('Could not find the requested attachment in JIRA', statusCode)
    }

}
