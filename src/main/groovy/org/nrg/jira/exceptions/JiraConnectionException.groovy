package org.nrg.jira.exceptions

class JiraConnectionException extends JiraZephyrException {

    JiraConnectionException(Throwable cause) {
        super('Could not connect to JIRA properly', cause)
    }

}
