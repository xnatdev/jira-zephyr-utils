package org.nrg.jira.exceptions

class VersionNotFoundException extends JiraZephyrException {

    VersionNotFoundException(int statusCode) {
        super('Could not find specified version in the JIRA project', statusCode)
    }

}
