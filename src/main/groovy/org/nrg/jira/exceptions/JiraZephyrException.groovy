package org.nrg.jira.exceptions

abstract class JiraZephyrException extends Exception {

    int statusCode

    JiraZephyrException(String explanation, int statusCode) {
        super("${explanation}. HTTP Status code: ${statusCode}.")
        this.statusCode = statusCode
    }

    JiraZephyrException(String message, Throwable cause) {
        super(message, cause)
    }

}
