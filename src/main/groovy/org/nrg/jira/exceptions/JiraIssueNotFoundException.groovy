package org.nrg.jira.exceptions

class JiraIssueNotFoundException extends JiraZephyrException {

    JiraIssueNotFoundException(int statusCode) {
        super('Could not find the specified issue or test', statusCode)
    }

}
