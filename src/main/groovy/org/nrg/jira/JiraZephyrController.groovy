package org.nrg.jira

import com.jayway.restassured.http.ContentType
import com.jayway.restassured.path.json.JsonPath
import com.jayway.restassured.response.Response
import org.nrg.jira.exceptions.AttachmentNotFound
import org.nrg.jira.exceptions.BadHttpResponseException
import org.nrg.jira.exceptions.CycleNotFoundException
import org.nrg.jira.exceptions.EntityNotFoundException
import org.nrg.jira.exceptions.JiraZephyrException
import org.nrg.jira.exceptions.LocalFileNotFoundException
import org.nrg.jira.exceptions.VersionNotFoundException
import org.nrg.jira.exceptions.ZapiNotFoundException
import org.nrg.jira.components.Attachment
import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.jira.components.zephyr.TestStep
import org.nrg.testing.HttpUtils

class JiraZephyrController extends JiraController {

    private static final String MULTIPART = 'multipart/form-data'

    JiraZephyrController(String jiraUrl, String username, String password, boolean verifyCredentials = true) throws JiraZephyrException {
        super(jiraUrl, username, password, verifyCredentials)

        if (!verifyCredentials) return

        final int zapiResponseCode = getCredentials().get(formatJiraUrl('/rest/zapi/latest/license')).statusCode
        if (zapiResponseCode != 200) {
            throw new ZapiNotFoundException(zapiResponseCode)
        }
    }

    /**
     * Returns Cycle object for the specified cycle
     * @param cycleName    Name of the cycle
     * @param projectKey   Key of the cycle's project
     * @param versionName  Fix version for the cycle
     * @return             Cycle object
     * @throws JiraZephyrException
     */
    Cycle getCycle(String cycleName, String projectKey, String versionName) throws JiraZephyrException {
        getCycleById(cycleName, getVersionId(projectKey, versionName))
    }

    Cycle getCycle(String cycleId) throws JiraZephyrException {
        final Response response = getCredentials().get(formatJiraUrl('/rest/zapi/latest/cycle', cycleId))
        if (response.statusCode == 200) {
            final Cycle cycle = response.as(Cycle)
            cycle.setId(cycleId)
            cycle.setExecutions(getExecutions(cycle))
            cycle
        } else {
            throw new CycleNotFoundException(response.statusCode)
        }
    }

    void downloadAttachment(Attachment attachment, File destinationFile) throws JiraZephyrException {
        final Response response = getCredentials().given().queryParams([id : attachment.fileId, name : attachment.fileName]).get(formatJiraUrl('/plugins/servlet/schedule/viewAttachment'))
        if (response.statusCode != 200) {
            throw new AttachmentNotFound(response.statusCode)
        }
        HttpUtils.saveBinaryResponseToFile(response, destinationFile)
    }

    /**
     * Creates a Zephyr test cycle
     * @param cycleName   Name for the cycle to be created
     * @param projectKey  Project key where the cycle will be created
     * @param versionName Fix version name for the cycle
     * @return            Object for the newly created cycle
     * @throws JiraZephyrException
     */
    Cycle createCycle(String cycleName, String projectKey, String versionName) throws JiraZephyrException {
        createCycleByIds(cycleName, getProjectId(projectKey), getVersionId(projectKey, versionName))
    }

    void updateBuildInfo(Cycle cycle, String build) throws JiraZephyrException {
        updateCycleField(cycle, 'build', build)
        cycle.setBuild(build)
    }

    void updateEnvironmentInfo(Cycle cycle, String environment) throws JiraZephyrException {
        updateCycleField(cycle, 'environment', environment)
        cycle.setEnvironment(environment)
    }

    /**
     * Creates executions and adds tests to cycle
     * @param cycle        Cycle object where tests will be added
     * @param testNumbers  List of test numbers to be added (e.g. [XNAT-100, XNAT-101])
     * @return             List of created executions
     * @throws JiraZephyrException
     */
    List<Execution> addTestsToCycle(Cycle cycle, List<String> testNumbers) throws JiraZephyrException {
        testNumbers.collect { testNumber ->
            createExecution(cycle, testNumber)
        }
    }

    /**
     * Creates a Zephyr test execution (delegates to {@link #createExecutionById(Cycle, String)} after getting the test's ID)
     * @param cycle       Cycle object from: {@link #createCycle(String, String, String)}
     * @param testNumber  JIRA test number (e.g. XNAT-100)
     * @return            Object for the created execution
     * @throws JiraZephyrException
     */
    Execution createExecution(Cycle cycle, String testNumber) throws JiraZephyrException {
        createExecutionById(cycle, getIssueId(testNumber))
    }

    /**
     * Updates Zephyr test execution status
     * @param execution Execution object, returned from: {@link #createExecution(Cycle, String)}
     * @param status      Test status
     * @throws JiraZephyrException
     */
    void updateExecutionStatus(Execution execution, TestStatus status) throws JiraZephyrException {
        updateExecution(execution, [status: status.jiraValue])
        execution.setStatus(status)
    }

    /**
     * Adds a comment to Zephyr test execution
     * @param execution Execution object, returned from: {@link #createExecution(Cycle, String)}
     * @param comment     Comment to add
     * @throws JiraZephyrException
     */
    void postExecutionComment(Execution execution, String comment) throws JiraZephyrException {
        final String truncated = truncateComment(comment)
        updateExecution(execution, [comment : truncated])
        execution.setComment(truncated)
    }

    /**
     * Links a defect to Zephyr test execution
     * @param execution Execution object, returned from: {@link #createExecution(Cycle, String)}
     * @param defect    Issue number/key (not ID) for the defect
     * @throws JiraZephyrException
     */
    void postDefect(Execution execution, String defect) throws JiraZephyrException {
        updateExecution(execution, [defectList : [defect], updateDefectList : true])
    }

    /**
     * Attaches a file to a Zephyr test execution
     * @param execution Execution object, returned from: {@link #createExecution(Cycle, String)}
     * @param attachment  File to upload
     * @throws JiraZephyrException
     */
    void postExecutionAttachment(Execution execution, File attachment) throws JiraZephyrException {
        postAttachment(attachment, execution.id, true)
        if (!execution.executionAttachments.any { it.fileName == attachment.name }) execution.executionAttachments << new Attachment(fileName: attachment.name)
    }

    /**
     * Attaches a file to a Zephyr test execution step
     * @param step        Step object, returned from: {@link #getSteps(Execution)}
     * @param attachment  File to upload
     * @throws JiraZephyrException
     */
    void postStepAttachment(TestStep step, File attachment) throws JiraZephyrException {
        postAttachment(attachment, step.id, false)
        if (!step.stepAttachments.any { it.fileName == attachment.name }) step.stepAttachments << new Attachment(fileName: attachment.name)
    }

    /**
     * Updates a Zephyr test execution step
     * @param step        Step object, returned from: {@link #getSteps(Execution)}
     * @param status      Test execution status for the step
     * @param comment     Comment to be added to the test step
     * @throws JiraZephyrException
     */
    void updateStepResult(TestStep step, TestStatus status, String comment) throws JiraZephyrException {
        final Map<String, Object> body = [:]
        if (status != null) {
            body.put('status', status.jiraValue)
            step.setStepStatus(status)
        }
        if (comment != null) {
            final String truncated = truncateComment(comment)
            body.put('comment', truncated)
            step.setComment(truncated)
        }
        final int statusCode = getCredentials().contentType(ContentType.JSON).body(body).put(formatJiraUrl('/rest/zapi/latest/stepResult', step.getId())).statusCode
        if (statusCode != 200) {
            throw new BadHttpResponseException('Could not update test step result', statusCode)
        }
    }

    private Cycle getCycleById(String cycleName, String versionId) throws JiraZephyrException {
        final String searchPath = "findAll {it.key != 'recordsCount'}.find {it.value.name == '${cycleName}'}"
        final Response response = getCredentials().given().queryParam('versionId', versionId).get(formatJiraUrl('/rest/zapi/latest/cycle'))
        if (response.statusCode == 400) {
            throw new VersionNotFoundException(400)
        }
        final JsonPath jsonPath = response.jsonPath()
        if (jsonPath.get(searchPath) == null) {
            throw new CycleNotFoundException(response.statusCode())
        }
        final Cycle cycle = jsonPath.getObject("${searchPath}.value", Cycle) as Cycle
        cycle.setId(jsonPath.getString("${searchPath}.key"))
        cycle.setExecutions(getExecutions(cycle))
        cycle
    }

    private List<Execution> getExecutions(Cycle cycle) throws JiraZephyrException {
        final List<Execution> executions = getCredentials().queryParam('cycleId', cycle.id).get(formatJiraUrl('/rest/zapi/latest/execution')).jsonPath().getObject('executions', Execution[])
        executions.sort { it.orderId }
        executions.each { execution ->
            extractTestInformationToExecution(execution)
            execution.setExecutionAttachments(getExecutionAttachments(execution))
            execution.setTestSteps(getSteps(execution))
        }
        executions
    }

    private void extractTestInformationToExecution(Execution execution) throws JiraZephyrException {
        execution.setTestDescription(getIssue(execution.issueId).description) // issue key and issue id can both be used to query this endpoint
    }

    private List<TestStep> getSteps(Execution execution) throws JiraZephyrException {
        final List<TestStep> stepsFromIssue = getCredentials().get(formatJiraUrl('/rest/zapi/latest/teststep', execution.issueId)).as(TestStep[])
        final List<TestStep> stepsFromExecution = getCredentials().queryParam('executionId', execution.id).get(formatJiraUrl('/rest/zapi/latest/stepResult')).as(TestStep[])
        // two separate representations because they each have incomplete information, which we synthesize. Correct order reflected in stepsFromIssue
        stepsFromIssue.each { issueStep ->
            final TestStep otherRepresentation = stepsFromExecution.find { it.stepId == issueStep.id }
            issueStep.setStepId(otherRepresentation.stepId)
            issueStep.setId(otherRepresentation.id)
            issueStep.setComment(otherRepresentation.comment)
            issueStep.setStepStatus(otherRepresentation.stepStatus)
            issueStep.setStepAttachments(getStepAttachments(issueStep))
        }
        stepsFromIssue
    }

    private List<Attachment> getAttachments(int entityId, String entityType) throws JiraZephyrException {
        final Response response = getCredentials().queryParams([entityId : entityId, entityType : entityType]).get(formatJiraUrl('/rest/zapi/latest/attachment/attachmentsByEntity'))
        if (response.statusCode != 200) {
            throw new EntityNotFoundException(response.statusCode)
        } else {
            final List<Attachment> attachments = response.jsonPath().getObject('data', Attachment[])
            attachments.sort()
        }
    }

    private List<Attachment> getExecutionAttachments(Execution execution) throws JiraZephyrException {
        getAttachments(execution.id, 'execution')
    }

    private List<Attachment> getStepAttachments(TestStep step) throws JiraZephyrException {
        getAttachments(step.id, 'TESTSTEPRESULT')
    }

    /**
     * Creates a Zephyr test cycle
     * @param cycleName Name for the cycle to be created
     * @param projectId JIRA internal ID for the project: {@link #getProjectId(String projectName)}
     * @param versionId JIRA internal ID for the version: {@link #getVersionId(String projectName, String versionName)}
     * @return          Object for the newly created cycle
     * @throws JiraZephyrException
     */
    private Cycle createCycleByIds(String cycleName, String projectId, String versionId) throws JiraZephyrException {
        final Response cycleResponse = getCredentials().contentType(ContentType.JSON).body([name: cycleName, projectId: projectId, versionId: versionId]).post(formatJiraUrl('/rest/zapi/latest/cycle'))
        final String cycleId = cycleResponse.path('id')
        if (cycleId != null) {
            getCycle(cycleId)
        } else {
            throw new BadHttpResponseException('Could not read id from cycle creation response', cycleResponse.statusCode)
        }
    }

    private void updateCycleField(Cycle cycle, String key, String value) {
        if (key != null && value != null) {
            final int statusCode = getCredentials().contentType(ContentType.JSON).body([id : cycle.id, versionId : cycle.versionId, (key) : value]).put(formatJiraUrl('/rest/zapi/latest/cycle')).statusCode
            if (statusCode != 200) {
                throw new BadHttpResponseException('Failed to update cycle information', statusCode)
            }
        }
    }

    /**
     * Creates a Zephyr test execution
     * @param cycle     Cycle object from: {@link #createCycle(String, String, String)}
     * @param testId    JIRA internal ID for the test: {@link #getIssueId(String)}
     * @return          Object for the created execution
     * @throws JiraZephyrException
     */
    private Execution createExecutionById(Cycle cycle, String testId) throws JiraZephyrException {
        final Response response = getCredentials().contentType(ContentType.JSON).body([cycleId : cycle.id, projectId : cycle.projectId, versionId : cycle.versionId, issueId : testId]).post(formatJiraUrl('/rest/zapi/latest/execution'))
        final Execution execution = response.jsonPath().getObject('values()[0]', Execution)
        extractTestInformationToExecution(execution)
        execution.setTestSteps(getSteps(execution))
        cycle.addExecution(execution)
        execution
    }

    private void updateExecution(Execution execution, Map<String, ?> body) {
        final int statusCode = getCredentials().contentType(ContentType.JSON).body(body).put(formatJiraUrl("/rest/zapi/latest/execution/${execution.id}/execute")).statusCode
        if (statusCode != 200) {
            throw new BadHttpResponseException('Could not update execution', statusCode)
        }
    }

    @SuppressWarnings('GrMethodMayBeStatic')
    private String truncateComment(String comment) {
        if (comment == null) {
            ''
        } else {
            (comment.length() > 500) ? comment.substring(0, 500) + '...' : comment
        }
    }

    private void postAttachment(File attachment, int entityId, boolean isExecution) throws JiraZephyrException {
        if (attachment == null || !attachment.exists()) {
            throw new LocalFileNotFoundException()
        }
        final Map queryParams = [entityId: entityId, entityType: (isExecution) ? 'execution' : 'stepresult']
        final int statusCode = getCredentials().queryParams(queryParams).header('X-Atlassian-Token', 'nocheck').contentType(MULTIPART).multiPart(attachment).post(formatJiraUrl('/rest/zapi/latest/attachment')).statusCode
        if (statusCode != 200) {
            throw new BadHttpResponseException('Failed to add attachment', statusCode)
        }
    }

}
