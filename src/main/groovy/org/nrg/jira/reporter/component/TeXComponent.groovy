package org.nrg.jira.reporter.component

interface TeXComponent {

    String generateTeX()

}