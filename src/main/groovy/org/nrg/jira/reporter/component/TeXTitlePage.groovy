package org.nrg.jira.reporter.component

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.reporter.TeXUtils
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.FileIOUtils

class TeXTitlePage implements TeXComponent {

    private static final String BASE_BUILD_SECTION = FileIOUtils.loadResource('build_section.tex').text
    private static final String BASE_ENVIRONMENT_SECTION = FileIOUtils.loadResource('environment_section.tex').text
    private static final String BASE_LOGO_SECTION = FileIOUtils.loadResource('logo_section.tex').text
    private static final String BASE_TITLE_PAGE = FileIOUtils.loadResource('title_page.tex').text
    private final Map<String, String> replacements = [:]

    TeXTitlePage(String project, Cycle cycle, String version, String jiraUrl, File logo) {
        replacements.put('%PROJECT%', project)
        replacements.put('%CYCLE%', TeXUtils.safeEscape(cycle.name))
        replacements.put('%VERSION%', TeXUtils.safeEscape(version))
        replacements.put('%CYCLE_LINK%', cycle.getCycleUrl(jiraUrl, project))
        replacements.put('%BUILD_SECTION%', (cycle.build == null) ? '' : BASE_BUILD_SECTION.replace('%BUILD%', cycle.build))
        final String environment = cycle.environment
        replacements.put('%ENVIRONMENT_SECTION%', (environment == null) ? '' : BASE_ENVIRONMENT_SECTION.replace('%ENVIRONMENT%', TeXUtils.isUrl(environment) ? "\\url{${environment}}" : environment))
        replacements.put('%LOGO_SECTION%', (logo != null && logo.exists()) ? BASE_LOGO_SECTION.replace('%LOGO%', logo.name) : '')
    }

    @Override
    String generateTeX() {
        CommonStringUtils.replaceRecursively(BASE_TITLE_PAGE, replacements)
    }

}
