package org.nrg.jira.reporter.component

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.testing.FileIOUtils

class TeXTableOfContents implements TeXComponent {

    private static final String BASE_TOC_SECTION = FileIOUtils.loadResource('table_of_contents.tex').text
    private final Cycle cycle

    TeXTableOfContents(Cycle cycle) {
        this.cycle = cycle
    }

    @Override
    String generateTeX() {
        BASE_TOC_SECTION.replace('%NON_PASSING%', generateContents())
    }

    private String generateContents() {
        final List<Execution> failing = cycle.executions.findAll { it.status == TestStatus.FAIL }
        final List<Execution> blocked = cycle.executions.findAll { it.status == TestStatus.BLOCKED }
        final Enumerate topLevelEnumerate = new Enumerate('(\\arabic*)')

        if (failing.isEmpty() && blocked.isEmpty()) {
            topLevelEnumerate << 'No tests are in \'Failed\' or \'Blocked\' status. Congratulations!'
        } else {
            if (!failing.isEmpty()) {
                addNonPassingListString(topLevelEnumerate, 'Failing', failing)
            }
            if (!blocked.isEmpty()) {
                addNonPassingListString(topLevelEnumerate, 'Blocked', blocked)
            }
        }
        topLevelEnumerate.generateTeX()
    }

    private static void addNonPassingListString(Enumerate topLevelEnumerate, String identifier, List<Execution> executions) {
        final Enumerate executionsEnumerate = new Enumerate('(\\arabic*)')
        executions.each { execution ->
            executionsEnumerate << "\\hyperref[subsec:${execution.issueKey}]{${execution.summary} (${execution.issueKey})}".toString()
        }
        topLevelEnumerate << "${identifier} Test Executions:\n${executionsEnumerate.generateTeX()}".toString()
    }

}
