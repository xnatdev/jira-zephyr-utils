package org.nrg.jira.reporter.component

import org.nrg.testing.FileIOUtils

class TeXFromResource implements TeXComponent {

    private final String file

    TeXFromResource(String fileWithoutExtension) {
        file = fileWithoutExtension
    }

    @Override
    String generateTeX() {
        FileIOUtils.loadResource("${file}.tex").text
    }

}
