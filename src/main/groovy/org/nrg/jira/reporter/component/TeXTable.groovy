package org.nrg.jira.reporter.component

import org.nrg.jira.reporter.TeXUtils

class TeXTable implements TeXComponent {

    List<String> lines

    TeXTable(String tableStructure) {
        lines = ["\\begin{longtable}{${tableStructure}} \\hline"]
    }

    void addRow(Object... elements) {
        lines << TeXUtils.indent(elements.join(' & ') + ' \\\\ \\hline')
    }

    @Override
    String generateTeX() {
        (lines + ['\\end{longtable}']).join('\n')
    }

}
