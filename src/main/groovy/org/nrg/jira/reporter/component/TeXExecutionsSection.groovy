package org.nrg.jira.reporter.component

import org.apache.commons.io.FilenameUtils
import org.apache.log4j.Logger
import org.nrg.jira.JiraZephyrController
import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.reporter.TeXUtils
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.FileIOUtils

import java.nio.file.Path

class TeXExecutionsSection implements TeXComponent {

    private static final Logger logger = Logger.getLogger(TeXExecutionsSection)
    private static final String BASE_EXECUTIONS_SECTION = FileIOUtils.loadResource('executions_section.tex').text
    private static final String BASE_EXECUTION = FileIOUtils.loadResource('execution_section.tex').text
    private static final List<String> IMAGE_EXTENSIONS = ['jpg', 'jpeg', 'png']
    private final Cycle cycle
    private final JiraZephyrController jiraZephyrController
    private final Path tempDirectory
    private final List<File> auxFiles

    TeXExecutionsSection(Cycle cycle, JiraZephyrController jiraZephyrController, Path tempDirectory, List<File> auxFiles) {
        this.cycle = cycle
        this.jiraZephyrController = jiraZephyrController
        this.tempDirectory = tempDirectory
        this.auxFiles = auxFiles
    }

    @Override
    String generateTeX() {
        BASE_EXECUTIONS_SECTION.replace('%EXECUTIONS%', readExecutions())
    }

    private String readExecutions() {
        cycle.executions.collect { execution ->
            CommonStringUtils.replaceEach(BASE_EXECUTION, [
                    '%SUMMARY%' : execution.summary,
                    '%ISSUE_KEY%' : execution.issueKey,
                    '%SUMMARY_TABLE%' : readExecutionSummaryTable(execution),
                    '%STEP_TABLE%' : readExecutionStepTable(execution)
            ])
        }.join('\n')
    }

    private String readExecutionSummaryTable(Execution execution) {
        final String description = execution.testDescription
        final String comment = execution.comment

        final TeXTable summaryTable = new TeXTable('| L{1.5in} | L{5in} |')
        summaryTable.addRow('Test name:', execution.summary)
        summaryTable.addRow('Test Key:', execution.issueKey)
        if (description != null && !description.isEmpty()) {
            summaryTable.addRow('Description:', TeXUtils.safeEscape(description))
        }
        summaryTable.addRow('Execution Status:', TeXUtils.colorCode(execution.status))
        summaryTable.addRow('Test Executor', execution.executedByDisplay)
        summaryTable.addRow('Execution Time:', execution.executedOn)
        if (comment != null && !comment.isEmpty()) {
            summaryTable.addRow('Comment:', comment)
        }
        summaryTable.generateTeX()
    }

    private String readExecutionStepTable(Execution execution) {
        final TeXTable stepResults = new TeXTable('| L{0.6in} | L{1.3in} | L{1.5in} | L{1in} | L{0.6in} | L{1in} |')
        stepResults.addRow('Step Number', 'Test Step', 'Test Data', 'Expected Result', 'Actual Result', 'Comment')

        execution.testSteps.each { step ->
            stepResults.addRow(step.orderId, TeXUtils.safeEscape(step.step), TeXUtils.formatTestStepDataString(step.data), TeXUtils.safeEscape(step.result), TeXUtils.colorCode(step.stepStatus), TeXUtils.safeEscape(step.comment))
            step.stepAttachments.eachWithIndex { attachment, screenshotIndex ->
                final String extension = FilenameUtils.getExtension(attachment.fileName)
                if (extension.toLowerCase() in IMAGE_EXTENSIONS) {
                    final File localFile = tempDirectory.resolve("${execution.issueKey}_${step.orderId}_${screenshotIndex}.${extension}").toFile()
                    jiraZephyrController.downloadAttachment(attachment, localFile)
                    auxFiles << localFile
                    stepResults.addRow("${step.orderId} (screenshots)", "\\multicolumn{5}{c|}{\\includegraphics[max width=4 in,valign=T]{${localFile.name}}}")
                }
            }
        }

        logger.info("Parsed test: ${execution.summary}")
        stepResults.generateTeX()
    }

}
