package org.nrg.jira.reporter.component

import org.nrg.jira.reporter.TeXUtils
import org.nrg.testing.FileIOUtils

class Enumerate extends ArrayList<String> implements TeXComponent {

    private static final String BASE_ENUMERATE = FileIOUtils.loadResource('enumerate.tex').text
    private final String label

    Enumerate(String label) {
        this.label = label
    }

    @Override
    String generateTeX() {
        BASE_ENUMERATE.replace('%LABEL%', label).replace('%CONTENT%', collect { TeXUtils.indent("\\item ${it}") }.join('\n'))
    }

}
