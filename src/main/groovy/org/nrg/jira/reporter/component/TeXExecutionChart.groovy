package org.nrg.jira.reporter.component

import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.components.zephyr.Execution
import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.FileIOUtils

class TeXExecutionChart implements TeXComponent {

    private static final String BASE_CHART = FileIOUtils.loadResource('execution_chart.tex').text
    private final Cycle cycle

    TeXExecutionChart(Cycle cycle) {
        this.cycle = cycle
    }

    @Override
    String generateTeX() {
        final List<Execution> executions = cycle.executions
        CommonStringUtils.replaceEach(BASE_CHART, [
                '%PASSING%' : sizeByStatus(executions, TestStatus.PASS),
                '%FAILING%' : sizeByStatus(executions, TestStatus.FAIL),
                '%BLOCKED%' : sizeByStatus(executions, TestStatus.BLOCKED),
                '%WIP%' : sizeByStatus(executions, TestStatus.WIP),
                '%UNEXEC%' : sizeByStatus(executions, TestStatus.UNEXECUTED)
        ])
    }

    private String sizeByStatus(List<Execution> executions, TestStatus status) {
        executions.findAll { it.status == status }.size()
    }

}
