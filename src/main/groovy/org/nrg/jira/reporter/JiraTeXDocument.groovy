package org.nrg.jira.reporter

import org.nrg.jira.reporter.component.TeXComponent

class JiraTeXDocument extends ArrayList<TeXComponent> {

    List<File> auxiliarySourceFiles = []

    String produceDocument() {
        this.collect { it.generateTeX() }.join('\n')
    }

}
