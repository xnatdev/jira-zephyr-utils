package org.nrg.jira.reporter

import org.nrg.jira.components.zephyr.TestStatus
import org.nrg.testing.CommonStringUtils

class TeXUtils {

    public static final String INDENT = '    '

    static String safeEscape(String input) {
        if (input == null) {
            return ''
        } else {
            CommonStringUtils.replaceRecursively(input, [
                    '\\' : '\\backslash',
                    '$' : '\\$',
                    '\\backslash' : '$\\backslash$',
                    '#' : '\\#',
                    '_' : '\\_',
                    '^' : '\\^{}',
                    '>' : '$>$',
                    '<' : '$<$',
                    '{' : '\\{',
                    '}' : '\\}',
                    '&' : '\\&',
                    '%' : '\\%',
                    '/' : '\\slash '
            ])
        }
    }

    static String indent(String textBlock, int times = 1) {
        textBlock.readLines().collect {
            "${INDENT * times}${it}"
        }.join('\n')
    }

    static int maxWordLength(String input) {
        if (input == null || input.isEmpty()) {
            0
        } else {
            input.replace('\n', ' ').split(' ').max {
                it.length()
            }.length()
        }
    }

    static boolean isUrl(String input) {
        try {
            new URL(input).toURI()
            true
        } catch (Exception ignored) {
            false
        }
    }

    static String colorCode(TestStatus status) {
        switch (status) {
            case TestStatus.PASS :
                return '\\textcolor{OliveGreen}{PASS}'
            case TestStatus.FAIL :
                return '\\textcolor{Red}{FAIL}'
            case TestStatus.UNEXECUTED :
                return 'UNEXEC.'
            case TestStatus.BLOCKED :
                return '\\textcolor{MidnightBlue}{BLOCKED}'
            case TestStatus.WIP :
                return '\\textcolor{Yellow}{IN PROGRESS}'
        }
    }

    static String formatTestStepDataString(String dataString) {
        final List<String> dataLines = safeEscape(dataString).split('\n')
        dataLines.collect { dataLine -> // use seqsplit package if the string is long, has long components, and only contains characters compatible with the package
            (dataLine.length() > 30 && maxWordLength(dataLine) > 15 && allCharactersAllowedForSeqsplit(dataLine)) ? "\\seqsplit{${dataLine.trim()}}" : dataLine.trim()
        }.join(', ')
    }

    static boolean allCharactersAllowedForSeqsplit(String input) {
        input.matches('[A-Za-z0-9\\\\{}._= ]+')
    }

}
