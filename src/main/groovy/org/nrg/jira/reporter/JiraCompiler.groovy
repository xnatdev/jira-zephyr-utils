package org.nrg.jira.reporter

import com.jayway.restassured.RestAssured
import com.jayway.restassured.response.Response
import org.nrg.jira.JiraZephyrController
import org.nrg.jira.components.zephyr.Cycle
import org.nrg.jira.reporter.component.TeXExecutionChart
import org.nrg.jira.reporter.component.TeXExecutionsSection
import org.nrg.jira.reporter.component.TeXFromResource
import org.nrg.jira.reporter.component.TeXTableOfContents
import org.nrg.jira.reporter.component.TeXTitlePage
import org.nrg.testing.CommonStringUtils
import org.nrg.testing.HttpUtils

import java.nio.file.Files
import java.nio.file.Path

class JiraCompiler {

    private final JiraZephyrController jiraZephyrController
    private final Cycle cycle
    private final String jiraUrl
    private final String project
    private final String version
    private final Path tempDirectory = Files.createTempDirectory('jirareporter')
    private final JiraTeXDocument teXDocument = new JiraTeXDocument()

    JiraCompiler(String jiraUrl, String username, String password, String project, String version, String cycleName) {
        this.jiraUrl = jiraUrl
        this.project = project
        this.version = version
        jiraZephyrController = new JiraZephyrController(jiraUrl, username, password)
        cycle = jiraZephyrController.getCycle(cycleName, project, version)
    }

    void compileAndBuild(boolean copySource) {
        teXDocument << new TeXFromResource('preamble')
        teXDocument << new TeXTitlePage(project, cycle, version, jiraUrl, downloadLogo(cycle.environment))
        teXDocument << new TeXTableOfContents(cycle)
        teXDocument << new TeXExecutionsSection(cycle, jiraZephyrController, tempDirectory, teXDocument.auxiliarySourceFiles)
        teXDocument << new TeXExecutionChart(cycle)
        teXDocument << new TeXFromResource('ending')
        new TeXManager(tempDirectory).compile(teXDocument, copySource)
    }

    private File downloadLogo(String environment) {
        if (environment != null && TeXUtils.isUrl(environment)) {
            try {
                final Response logoPathResponse = RestAssured.with().relaxedHTTPSValidation().get(CommonStringUtils.formatUrl(environment, '/xapi/siteConfig/siteLogoPath'))
                if (logoPathResponse.statusCode() != 200) {
                    return null
                }
                final String logoPath = logoPathResponse.asString()
                final String fileName = logoPath.split('/').last()
                final Response logoResponse = RestAssured.with().relaxedHTTPSValidation().get(CommonStringUtils.formatUrl(environment, logoPath))
                if (logoResponse.statusCode != 200) {
                    return null
                }
                final File logo = tempDirectory.resolve(fileName).toFile()
                HttpUtils.saveBinaryResponseToFile(logoResponse, logo)
                teXDocument.auxiliarySourceFiles << logo
                return logo
            } catch (Exception ignored) {
                return null
            }
        }
        return null
    }

}
