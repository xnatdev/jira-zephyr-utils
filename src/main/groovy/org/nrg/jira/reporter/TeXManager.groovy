package org.nrg.jira.reporter

import org.nrg.testing.TimeUtils

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class TeXManager {

    private static final String TEX_BASE_NAME = 'jira_cycle_document'
    private static final String TEX_FILE_NAME = "${TEX_BASE_NAME}.tex"
    private static final String TEX_PDF_NAME  = "${TEX_BASE_NAME}.pdf"
    private static final Path OUTPUT_DIR = Paths.get('.', "extracted_jira_cycle_${TimeUtils.getTimestamp()}")
    private final Path tempDirectory

    TeXManager(Path tempDirectory) {
        this.tempDirectory = tempDirectory
    }

    void compile(JiraTeXDocument document, boolean copySource) {
        final File sourceTeXFile = tempDirectory.resolve(TEX_FILE_NAME).toFile()
        sourceTeXFile.createNewFile()
        sourceTeXFile << document.produceDocument()
        document.auxiliarySourceFiles << sourceTeXFile

        3.times {
            final StringBuilder stdOut = new StringBuilder()
            final StringBuilder stdErr = new StringBuilder()
            final ProcessBuilder processBuilder = new ProcessBuilder('pdflatex', '-halt-on-error', sourceTeXFile.name)
            processBuilder.directory(tempDirectory.toFile())
            final Process pdfLatex = processBuilder.start()
            pdfLatex.consumeProcessOutput(stdOut, stdErr)
            pdfLatex.waitForOrKill(60000)
        } // 3 compilations from table of contents and page numbers

        OUTPUT_DIR.toFile().mkdir()

        if (!copySource){
            document.auxiliarySourceFiles.clear()
        }

        document.auxiliarySourceFiles << tempDirectory.resolve(TEX_PDF_NAME).toFile()

        document.auxiliarySourceFiles.each { file ->
            Files.copy(file.toPath(), OUTPUT_DIR.resolve(file.name))
        }
    }

}
